package com.sidyeti.camerastatus

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.firebase.jobdispatcher.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG = "CAMERA_CHECK_SERVICE"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(applicationContext))
        startButton.setOnClickListener { createJob(dispatcher) }
        stopButton.setOnClickListener { cancelJob(dispatcher) }
    }

    private fun createJob(dispatcher: FirebaseJobDispatcher) {
        val myJob = dispatcher.newJobBuilder()
                .setLifetime(Lifetime.FOREVER)
                .setService(CameraCheckService::class.java)
                .setTag(TAG)
                .setReplaceCurrent(true)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(5, 10))
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build()
        dispatcher.mustSchedule(myJob)
        Toast.makeText(applicationContext,"JOB created",Toast.LENGTH_SHORT).show()
    }

    private fun cancelJob(dispatcher: FirebaseJobDispatcher){
        dispatcher.cancel(TAG)
        Toast.makeText(applicationContext,"JOB cancelled",Toast.LENGTH_SHORT).show()
    }
}
