package com.sidyeti.camerastatus

import android.util.Log
import android.widget.Toast
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService

class CameraCheckService : JobService() {

    private val TAG = CameraCheckService::class.simpleName
    override fun onStartJob(job: JobParameters?): Boolean {
      Thread(
              Runnable {
                 // Toast.makeText(applicationContext,"Camera checking :)",Toast.LENGTH_SHORT).show()
                  Log.d(TAG,"running")
                  Thread.sleep(2000)
                  Log.d(TAG,"finished")
              }
          ).start()
//        Log.d(TAG,"running")
//        Thread.sleep(2000)
//        Log.d(TAG,"finished")
        return false
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        return false
    }

}
